import React from "react";

class PresentationForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      presenterName: "",
      companyName: "",
      presenterEmail: "",
      title: "",
      synopsis: "",
      conferences: [],
    };
  this.handleSubmit = this.handleSubmit.bind(this);
  }

  async componentDidMount() {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      this.setState({ conferences: data.conferences });
      // console.log(data.conferences);
    }
  }

  // handle submission
  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    data.presenter_name = data.presenterName;
    data.company_name = data.companyName;
    data.presenter_email = data.presenterEmail;
    delete data.presenterName;
    delete data.companyName;
    delete data.presenterEmail;
    delete data.conferences;
    console.log(data)

    const presentationUrl = `http://localhost:8000${data.conference}presentations/`;
    const fetchOptions = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const presentationResponse = await fetch(presentationUrl, fetchOptions);
    if (presentationResponse.ok) {
      this.setState({
        presenterName: "",
        companyName: "",
        presenterEmail: "",
        title: "",
        synopsis: "",
        conference: "",
      });
    }
  }

  // states: presenter_name, company_name, presenter_email, title, synopsis, created
// different approach to do the handle_changes
  handleInputChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  render() {
    return (
      <div className="row">
         <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form onSubmit={this.handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.presenterName}
                  placeholder="presenter name"
                  required
                  type="text"
                  id="presenterName"
                  name="presenterName"
                  className="form-control"
                />
                <label htmlFor="presenterName">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.presenterEmail}
                  placeholder="presenter_email count"
                  required
                  type="email"
                  id="presenterEmail"
                  name="presenterEmail"
                  className="form-control"
                />
                <label htmlFor="presenterEmail">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.companyName}
                  placeholder="companyName"

                  type="text"
                  id="companyName"
                  name="companyName"
                  className="form-control"
                />
                <label htmlFor="companyName">Company Name (optional)</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.title}
                  placeholder="title"
                  required
                  type="text"
                  id="title"
                  name="title"
                  className="form-control"
                />
                <label htmlFor="title">Title of Presentation</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea
                  onChange={this.handleInputChange}
                  value={this.state.synopsis}
                  required
                  type="text"
                  id="synopsis"
                  name="synopsis"
                  className="form-control"
                  rows="3"
                ></textarea>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.conference}
                  required
                  id="conference"
                  name="conference"
                  className="form-select"
                >
                  <option value="">Select a Conference</option>
                  {this.state.conferences.map((conference) => {
                    return (
                      <option key={conference.href} value={conference.href}>
                        {conference.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default PresentationForm;
